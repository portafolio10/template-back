(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["template-detail-template-detail-module"],{

/***/ "./node_modules/file-saver/dist/FileSaver.min.js":
/*!*******************************************************!*\
  !*** ./node_modules/file-saver/dist/FileSaver.min.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;(function(a,b){if(true)!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_FACTORY__ = (b),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));else {}})(this,function(){"use strict";function b(a,b){return"undefined"==typeof b?b={autoBom:!1}:"object"!=typeof b&&(console.warn("Deprecated: Expected third argument to be a object"),b={autoBom:!b}),b.autoBom&&/^\s*(?:text\/\S*|application\/xml|\S*\/\S*\+xml)\s*;.*charset\s*=\s*utf-8/i.test(a.type)?new Blob(["\uFEFF",a],{type:a.type}):a}function c(a,b,c){var d=new XMLHttpRequest;d.open("GET",a),d.responseType="blob",d.onload=function(){g(d.response,b,c)},d.onerror=function(){console.error("could not download file")},d.send()}function d(a){var b=new XMLHttpRequest;b.open("HEAD",a,!1);try{b.send()}catch(a){}return 200<=b.status&&299>=b.status}function e(a){try{a.dispatchEvent(new MouseEvent("click"))}catch(c){var b=document.createEvent("MouseEvents");b.initMouseEvent("click",!0,!0,window,0,0,0,80,20,!1,!1,!1,!1,0,null),a.dispatchEvent(b)}}var f="object"==typeof window&&window.window===window?window:"object"==typeof self&&self.self===self?self:"object"==typeof global&&global.global===global?global:void 0,a=f.navigator&&/Macintosh/.test(navigator.userAgent)&&/AppleWebKit/.test(navigator.userAgent)&&!/Safari/.test(navigator.userAgent),g=f.saveAs||("object"!=typeof window||window!==f?function(){}:"download"in HTMLAnchorElement.prototype&&!a?function(b,g,h){var i=f.URL||f.webkitURL,j=document.createElement("a");g=g||b.name||"download",j.download=g,j.rel="noopener","string"==typeof b?(j.href=b,j.origin===location.origin?e(j):d(j.href)?c(b,g,h):e(j,j.target="_blank")):(j.href=i.createObjectURL(b),setTimeout(function(){i.revokeObjectURL(j.href)},4E4),setTimeout(function(){e(j)},0))}:"msSaveOrOpenBlob"in navigator?function(f,g,h){if(g=g||f.name||"download","string"!=typeof f)navigator.msSaveOrOpenBlob(b(f,h),g);else if(d(f))c(f,g,h);else{var i=document.createElement("a");i.href=f,i.target="_blank",setTimeout(function(){e(i)})}}:function(b,d,e,g){if(g=g||open("","_blank"),g&&(g.document.title=g.document.body.innerText="downloading..."),"string"==typeof b)return c(b,d,e);var h="application/octet-stream"===b.type,i=/constructor/i.test(f.HTMLElement)||f.safari,j=/CriOS\/[\d]+/.test(navigator.userAgent);if((j||h&&i||a)&&"undefined"!=typeof FileReader){var k=new FileReader;k.onloadend=function(){var a=k.result;a=j?a:a.replace(/^data:[^;]*;/,"data:attachment/file;"),g?g.location.href=a:location=a,g=null},k.readAsDataURL(b)}else{var l=f.URL||f.webkitURL,m=l.createObjectURL(b);g?g.location=m:location.href=m,g=null,setTimeout(function(){l.revokeObjectURL(m)},4E4)}});f.saveAs=g.saveAs=g, true&&(module.exports=g)});

//# sourceMappingURL=FileSaver.min.js.map

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/template-detail/template-detail.component.html":
/*!******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/template-detail/template-detail.component.html ***!
  \******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"full-page login-page\">\n\t<div class=\"content\">\n\t\t<div class=\"container\">\n\t\t\t<div class=\"row mb-4\">\n\t\t\t\t<div class=\"col-md-4\">\n\t\t\t\t\t<label class=\"form-label\">Identificador de la plantilla</label>\n\t\t\t\t\t<input type=\"text\" class=\"form-control\" [(ngModel)]=\"template.name\" readonly>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"row\" *ngFor=\"let attr of template.attrs\">\n\t\t\t\t<div class=\"col-md-4\">\n\t\t\t\t\t<label class=\"form-label\">Nombre</label>\n\t\t\t\t\t<input type=\"email\" class=\"form-control\" [(ngModel)]=\"attr.name\" readonly>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-md-4\">\n\t\t\t\t\t<label class=\"form-label\">Value</label>\n\t\t\t\t\t<input type=\"email\" class=\"form-control\" [(ngModel)]=\"attr.value\" (ngModelChange)=\"modelChanged(attr)($event)\">\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"row mt-4\">\n\t\t\t\t<div class=\"col-md-12\">\n\t\t\t\t\t<div class=\"bg-light p-4 rounded\">\n\t\t\t\t\t\t<label>Contenido de la plantilla</label>\n\t\t\t\t\t\t<pre style=\"color: #525f7f;\">{{template._contentParse}}</pre>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"row mt-4\">\n\t\t\t\t<div class=\"col-md-12\">\n\t\t\t\t\t<button class=\"btn btn-primary\" [disabled]=\"!schemaValid\" (click)=\"renderTemplate()\">Renderizar plantilla</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>");

/***/ }),

/***/ "./src/app/template-detail/template-detail-routing.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/template-detail/template-detail-routing.module.ts ***!
  \*******************************************************************/
/*! exports provided: TemplateDetailRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TemplateDetailRoutingModule", function() { return TemplateDetailRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _template_detail_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./template-detail.component */ "./src/app/template-detail/template-detail.component.ts");




var routes = [
    {
        path: "",
        component: _template_detail_component__WEBPACK_IMPORTED_MODULE_3__["TemplateDetailComponent"]
    }
];
var TemplateDetailRoutingModule = /** @class */ (function () {
    function TemplateDetailRoutingModule() {
    }
    TemplateDetailRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], TemplateDetailRoutingModule);
    return TemplateDetailRoutingModule;
}());



/***/ }),

/***/ "./src/app/template-detail/template-detail.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/template-detail/template-detail.component.scss ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RlbXBsYXRlLWRldGFpbC90ZW1wbGF0ZS1kZXRhaWwuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/template-detail/template-detail.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/template-detail/template-detail.component.ts ***!
  \**************************************************************/
/*! exports provided: TemplateDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TemplateDetailComponent", function() { return TemplateDetailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_template_services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/template.services */ "./src/app/services/template.services.ts");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! file-saver */ "./node_modules/file-saver/dist/FileSaver.min.js");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(file_saver__WEBPACK_IMPORTED_MODULE_4__);





var TemplateDetailComponent = /** @class */ (function () {
    function TemplateDetailComponent(templateServices, activateRoute) {
        this.templateServices = templateServices;
        this.activateRoute = activateRoute;
    }
    TemplateDetailComponent.prototype.ngOnInit = function () {
        this.setTemplateId(this.activateRoute.snapshot.params.id);
        this.setTemplate({ attrs: [], contentBase64: '' });
        this.getTemplate(this.templateId);
    };
    TemplateDetailComponent.prototype.setTemplateId = function (id) {
        this.templateId = id;
    };
    TemplateDetailComponent.prototype.setTemplate = function (template) {
        this.template = template;
        this.template.attrs = this.template.attrs.map(function (attr) {
            attr.value = "";
            return attr;
        });
        this.template._contentParse = atob(this.template.contentBase64);
    };
    TemplateDetailComponent.prototype.getTemplate = function (id) {
        this.templateServices.find({ id: id }).subscribe({
            next: this.getTemplateOk.bind(this),
            error: this.getTemplateErr.bind(this)
        });
    };
    TemplateDetailComponent.prototype.getTemplateOk = function (response) {
        console.log('getTemplateOk', response);
        this.setTemplate(response);
    };
    TemplateDetailComponent.prototype.getTemplateErr = function (errr) {
        console.log('getTemplateErr', errr);
    };
    TemplateDetailComponent.prototype.modelChanged = function (attr) {
        var _this = this;
        var template = atob(this.template.contentBase64);
        return function (attrValue) {
            _this.template._contentParse = template.replace("{{" + attr.name + "}}", attrValue);
        };
    };
    Object.defineProperty(TemplateDetailComponent.prototype, "schemaValid", {
        get: function () {
            return this.template.attrs.filter(function (attr) { return attr.value != ""; }).length == this.template.attrs.length;
        },
        enumerable: false,
        configurable: true
    });
    TemplateDetailComponent.prototype.renderTemplate = function () {
        var attrs = this.template.attrs.reduce(function (carry, attr) {
            carry[attr.name] = attr.value;
            return carry;
        }, {});
        this.renderTemplateAction({
            templateId: this.templateId,
            templateParams: JSON.stringify(attrs)
        });
    };
    TemplateDetailComponent.prototype.renderTemplateAction = function (command) {
        this.templateServices.render(command)
            .subscribe({
            next: this.renderTemplateOk.bind(this),
            error: this.renderTemplateErr.bind(this)
        });
    };
    TemplateDetailComponent.prototype.renderTemplateOk = function (response) {
        console.log("renderTemplateOk");
        Object(file_saver__WEBPACK_IMPORTED_MODULE_4__["saveAs"])(response, this.template.name.concat('.pdf'));
    };
    TemplateDetailComponent.prototype.renderTemplateErr = function (errr) {
        console.log("renderTemplateErr", errr);
    };
    TemplateDetailComponent.ctorParameters = function () { return [
        { type: _services_template_services__WEBPACK_IMPORTED_MODULE_3__["TemplateServices"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }
    ]; };
    TemplateDetailComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-template-detail',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./template-detail.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/template-detail/template-detail.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./template-detail.component.scss */ "./src/app/template-detail/template-detail.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_services_template_services__WEBPACK_IMPORTED_MODULE_3__["TemplateServices"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], TemplateDetailComponent);
    return TemplateDetailComponent;
}());



/***/ }),

/***/ "./src/app/template-detail/template-detail.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/template-detail/template-detail.module.ts ***!
  \***********************************************************/
/*! exports provided: TemplateDetailModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TemplateDetailModule", function() { return TemplateDetailModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _template_detail_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./template-detail-routing.module */ "./src/app/template-detail/template-detail-routing.module.ts");
/* harmony import */ var _template_detail_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./template-detail.component */ "./src/app/template-detail/template-detail.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _form_components_form_components_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../form-components/form-components.module */ "./src/app/form-components/form-components.module.ts");







var TemplateDetailModule = /** @class */ (function () {
    function TemplateDetailModule() {
    }
    TemplateDetailModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_template_detail_component__WEBPACK_IMPORTED_MODULE_4__["TemplateDetailComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _template_detail_routing_module__WEBPACK_IMPORTED_MODULE_3__["TemplateDetailRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _form_components_form_components_module__WEBPACK_IMPORTED_MODULE_6__["FormComponentsModule"]
            ]
        })
    ], TemplateDetailModule);
    return TemplateDetailModule;
}());



/***/ })

}]);
//# sourceMappingURL=template-detail-template-detail-module.js.map