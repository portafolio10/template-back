(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["template-factory-template-factory-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/template-factory/template-factory.component.html":
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/template-factory/template-factory.component.html ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"full-page login-page\">\n\t<div class=\"content\">\n\t\t<div class=\"container\">\n\t\t\t<div class=\"row mb-4\">\n\t\t\t\t<div class=\"col-md-4\">\n\t\t\t\t\t<label class=\"form-label\">Identificador de la plantilla</label>\n\t\t\t\t\t<input type=\"email\" class=\"form-control\" [(ngModel)]=\"nameTemplate\">\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"d-flex justify-content-between mb-2\">\n\t\t\t\t<app-nazz-input-file [sizeMax]=\"50000000\" [type]=\"['html']\" (fileSelected)=\"fileSelected($event)\"\n\t\t\t\t\t(onRead)=\"onReadFile($event)\" [key]=\"'file'\"></app-nazz-input-file>\n\t\t\t\t<button class=\"btn btn-secondary\" (click)=\"agregarAttr()\" [disabled]=\"!contentTemplate.length\">Agregar atributo</button>\n\t\t\t</div>\n\t\t\t<div class=\"row\" *ngFor=\"let attr of attrs\">\n\t\t\t\t<div class=\"col-md-4\">\n\t\t\t\t\t<label class=\"form-label\">Nombre</label>\n\t\t\t\t\t<input type=\"email\" class=\"form-control\" [(ngModel)]=\"attr.name\" (ngModelChange)=\"modelChanged(attr)($event)\">\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-md-4\">\n\t\t\t\t\t<label>Tipo</label>\n\t\t\t\t\t<select class=\"custom-select\" [(ngModel)]=\"attr.type\">\n\t\t\t\t\t\t<option *ngFor=\"let type of types\" [value]=\"type.value\" style=\"color: #525f7f;\">{{type.value}}</option>\n\t\t\t\t\t</select>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"d-flex flex-column col-md-2\">\n\t\t\t\t\t<label>&nbsp;</label>\n\t\t\t\t\t<button class=\"btn btn-secondary btn-sm\" (click)=\"borrarAttr(attr.id)\">X</button>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-md-12\" style=\"color: rgba(255, 60, 0, 0.849); font-size: .8rem;\">\n\t\t\t\t\t{{attr.error}}\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"row mt-4\">\n\t\t\t\t<div class=\"col-md-12\">\n\t\t\t\t\t<div class=\"bg-light p-4 rounded\">\n\t\t\t\t\t\t<label>Contenido de la plantilla</label>\n\t\t\t\t\t\t<pre style=\"color: #525f7f;\">{{contentTemplate}}</pre>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"row mt-4\">\n\t\t\t\t<div class=\"col-md-12\">\n\t\t\t\t\t<button class=\"btn btn-primary\" [disabled]=\"!schemaValid\" (click)=\"createTemplate()\">Crear plantilla</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>");

/***/ }),

/***/ "./src/app/template-factory/template-factory-routing.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/template-factory/template-factory-routing.module.ts ***!
  \*********************************************************************/
/*! exports provided: TemplateFactoryRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TemplateFactoryRoutingModule", function() { return TemplateFactoryRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _template_factory_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./template-factory.component */ "./src/app/template-factory/template-factory.component.ts");




var routes = [
    {
        path: "",
        component: _template_factory_component__WEBPACK_IMPORTED_MODULE_3__["TemplateFactoryComponent"]
    }
];
var TemplateFactoryRoutingModule = /** @class */ (function () {
    function TemplateFactoryRoutingModule() {
    }
    TemplateFactoryRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], TemplateFactoryRoutingModule);
    return TemplateFactoryRoutingModule;
}());



/***/ }),

/***/ "./src/app/template-factory/template-factory.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/template-factory/template-factory.component.scss ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RlbXBsYXRlLWZhY3RvcnkvdGVtcGxhdGUtZmFjdG9yeS5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/template-factory/template-factory.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/template-factory/template-factory.component.ts ***!
  \****************************************************************/
/*! exports provided: TemplateFactoryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TemplateFactoryComponent", function() { return TemplateFactoryComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_template_services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/template.services */ "./src/app/services/template.services.ts");




var TemplateFactoryComponent = /** @class */ (function () {
    function TemplateFactoryComponent(router, templateServices) {
        this.router = router;
        this.templateServices = templateServices;
        this.nameTemplate = "";
        this.contentBase64Template = "";
        this.contentTemplate = "";
        this.fileTemplate = "";
        this.attrs = [];
        this.types = [];
    }
    TemplateFactoryComponent.prototype.ngOnInit = function () {
        this.setTypes([
            { id: 1, value: 'integer' },
            { id: 2, value: 'number' },
            { id: 3, value: 'string' }
        ]);
    };
    TemplateFactoryComponent.prototype.setContentTemplate = function (contentTemplate) {
        this.contentTemplate = contentTemplate;
    };
    TemplateFactoryComponent.prototype.setTypes = function (types) {
        this.types = types;
    };
    TemplateFactoryComponent.prototype.setFileTemplate = function (fileTemplate) {
        this.fileTemplate = fileTemplate;
    };
    TemplateFactoryComponent.prototype.setContentBase64Template = function (contentBase64Template) {
        this.contentBase64Template = contentBase64Template;
    };
    TemplateFactoryComponent.prototype.onReadFile = function ($event) {
        var contentBase64Template = this.getContentFromTemplateBase64($event.stream);
        var contentTemplate = this.getContentFromTemplate($event.stream);
        this.setContentBase64Template(contentBase64Template);
        this.setContentTemplate(contentTemplate);
    };
    TemplateFactoryComponent.prototype.fileSelected = function (fileSelected) {
        if (!fileSelected) {
            this.attrs = [];
            this.contentTemplate = "";
            return;
        }
        this.setFileTemplate(fileSelected);
    };
    TemplateFactoryComponent.prototype.agregarAttr = function () {
        var id = this.attrs.length + 1;
        this.attrs.push({
            id: id,
            type: '',
            name: ''
        });
    };
    TemplateFactoryComponent.prototype.borrarAttr = function (id) {
        this.attrs = this.attrs.filter(function (x) { return x.id != id; });
    };
    TemplateFactoryComponent.prototype.getContentFromTemplateBase64 = function (contentBase64) {
        var contentBase64ToArray = contentBase64.split('base64,');
        var htmlBase64 = contentBase64ToArray[1];
        return htmlBase64;
    };
    TemplateFactoryComponent.prototype.getContentFromTemplate = function (contentBase64) {
        return atob(this.getContentFromTemplateBase64(contentBase64));
    };
    TemplateFactoryComponent.prototype.modelChanged = function (attr) {
        var _this = this;
        return function (attrName) {
            var attrMustache = "{{" + attrName + "}}";
            var attrsExistIntoTemplate = _this.contentTemplate.indexOf(attrMustache) > -1;
            if (!attrsExistIntoTemplate) {
                attr.error = "El atributo {{" + attrName + "}} No existe en la plantilla";
                return;
            }
            attr.error = "";
        };
    };
    Object.defineProperty(TemplateFactoryComponent.prototype, "schemaValid", {
        get: function () {
            var schemaValid = this.attrs.filter(function (attr) {
                var rules = [attr.error == "", attr.name != "", attr.type != ""];
                return rules.filter(function (rule) { return rule; }).length === rules.length;
            });
            var schemaValidPerfectMatch = this.attrs.length === schemaValid.length;
            var contentTemplateIsLoad = this.contentTemplate != "";
            var hasSomeOneAttr = this.attrs.length;
            var hasNameTemplate = this.nameTemplate != "";
            return schemaValidPerfectMatch && contentTemplateIsLoad && hasSomeOneAttr && hasNameTemplate;
        },
        enumerable: false,
        configurable: true
    });
    TemplateFactoryComponent.prototype.request = function () {
        var attrsTemplate = this.attrs.map(function (attr) { return attr.name; });
        var schemaTemplate = this.createSchema(this.attrs);
        return this.obj({
            nameTemplate: this.nameTemplate,
            contentTemplate: this.contentTemplate.replace(/(\r\n|\n|\r)/gm, ""),
            contentBase64Template: this.contentBase64Template,
            //fileTemplate: this.fileTemplate,
            attrsTemplate: attrsTemplate,
            schemaTemplate: JSON.stringify(schemaTemplate)
        });
    };
    TemplateFactoryComponent.prototype.obj = function (paramsObj) {
        return Object.assign(Object.create(null), paramsObj);
    };
    TemplateFactoryComponent.prototype.createSchema = function (attrs) {
        var schemaTemplate = attrs.reduce(function (schema, attr) {
            schema.properties[attr.name] = {
                type: attr.type
            };
            return schema;
        }, this.obj({ properties: {} }));
        return schemaTemplate;
    };
    TemplateFactoryComponent.prototype.createTemplate = function () {
        console.log("request");
        this.templateServices.create(this.request()).subscribe({
            next: this.createTemplateOk.bind(this),
            error: this.createTemplateErr.bind(this)
        });
    };
    TemplateFactoryComponent.prototype.createTemplateOk = function (response) {
        console.log("createTemplateOk", response);
        this.router.navigate(['/template-list']);
    };
    TemplateFactoryComponent.prototype.createTemplateErr = function (err) {
        console.log("createTemplateErr", err);
    };
    TemplateFactoryComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _services_template_services__WEBPACK_IMPORTED_MODULE_3__["TemplateServices"] }
    ]; };
    TemplateFactoryComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-template-factory',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./template-factory.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/template-factory/template-factory.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./template-factory.component.scss */ "./src/app/template-factory/template-factory.component.scss")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _services_template_services__WEBPACK_IMPORTED_MODULE_3__["TemplateServices"]])
    ], TemplateFactoryComponent);
    return TemplateFactoryComponent;
}());



/***/ }),

/***/ "./src/app/template-factory/template-factory.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/template-factory/template-factory.module.ts ***!
  \*************************************************************/
/*! exports provided: TemplateFactoryModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TemplateFactoryModule", function() { return TemplateFactoryModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _template_factory_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./template-factory-routing.module */ "./src/app/template-factory/template-factory-routing.module.ts");
/* harmony import */ var _template_factory_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./template-factory.component */ "./src/app/template-factory/template-factory.component.ts");
/* harmony import */ var _form_components_form_components_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../form-components/form-components.module */ "./src/app/form-components/form-components.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");







var TemplateFactoryModule = /** @class */ (function () {
    function TemplateFactoryModule() {
    }
    TemplateFactoryModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_template_factory_component__WEBPACK_IMPORTED_MODULE_4__["TemplateFactoryComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
                _template_factory_routing_module__WEBPACK_IMPORTED_MODULE_3__["TemplateFactoryRoutingModule"],
                _form_components_form_components_module__WEBPACK_IMPORTED_MODULE_5__["FormComponentsModule"]
            ]
        })
    ], TemplateFactoryModule);
    return TemplateFactoryModule;
}());



/***/ })

}]);
//# sourceMappingURL=template-factory-template-factory-module.js.map