package com.carpeta.ingest;

import javax.annotation.Resource;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.ErrorPage;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
//import org.springframework.scheduling.annotation.EnableScheduling;

import com.carpeta.ingest.handlers.fileStore.IFileStore;

@SpringBootApplication
//@EnableScheduling
public class ProyectoCarpetaIngestApplication implements CommandLineRunner {

	@Resource
	IFileStore fileStore;

	public static void main(String[] args) {
		SpringApplication.run(ProyectoCarpetaIngestApplication.class, args);
	}

	@Override
	public void run(String... arg) throws Exception {
		fileStore.deleteAll();
		fileStore.init();
	}
	
	@Bean
	public ConfigurableServletWebServerFactory webServerFactory() {
		TomcatServletWebServerFactory factory = new TomcatServletWebServerFactory();
		factory.addErrorPages(new ErrorPage(HttpStatus.NOT_FOUND, "/index.html"));
		return factory;
	}	
}
