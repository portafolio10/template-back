package com.carpeta.ingest.requests;

import java.util.List;

public class TemplateCreateRequest {
	
	public String nameTemplate;
	public String contentTemplate;
	public String contentBase64Template;
	public List<String> attrsTemplate; 
	public String schemaTemplate;
	
	public TemplateCreateRequest() {
		super();
	}
	public String getNameTemplate() {
		return nameTemplate;
	}
	public void setNameTemplate(String nameTemplate) {
		this.nameTemplate = nameTemplate;
	}
	public String getContentTemplate() {
		return contentTemplate;
	}
	public void setContentTemplate(String contentTemplate) {
		this.contentTemplate = contentTemplate;
	}
	public String getContentBase64Template() {
		return contentBase64Template;
	}
	public void setContentBase64Template(String contentBase64Template) {
		this.contentBase64Template = contentBase64Template;
	}
	public List<String> getAttrsTemplate() {
		return attrsTemplate;
	}
	public void setAttrsTemplate(List<String> attrsTemplate) {
		this.attrsTemplate = attrsTemplate;
	}
	public String getSchemaTemplate() {
		return schemaTemplate;
	}
	public void setSchemaTemplate(String schemaTemplate) {
		this.schemaTemplate = schemaTemplate;
	}
	
}
