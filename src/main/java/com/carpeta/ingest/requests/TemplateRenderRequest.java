package com.carpeta.ingest.requests;

public class TemplateRenderRequest {
	protected Long templateId;
	protected String templateParams;
	public TemplateRenderRequest() {
	}
	public Long getTemplateId() {
		return templateId;
	}
	public void setTemplateId(Long templateId) {
		this.templateId = templateId;
	}
	public String getTemplateParams() {
		return templateParams;
	}
	public void setTemplateParams(String templateParams) {
		this.templateParams = templateParams;
	}
	
}
