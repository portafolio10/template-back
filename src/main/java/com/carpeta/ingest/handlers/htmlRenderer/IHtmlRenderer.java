package com.carpeta.ingest.handlers.htmlRenderer;

import java.util.Map;

public interface IHtmlRenderer {
	public String render(String htmlTemplateString, Map<String, Object> data);
}
