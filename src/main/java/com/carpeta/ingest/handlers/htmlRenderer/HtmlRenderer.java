package com.carpeta.ingest.handlers.htmlRenderer;

import java.util.Map;

import org.springframework.stereotype.Service;
import com.samskivert.mustache.Mustache;
import com.samskivert.mustache.Template;

@Service
public class HtmlRenderer implements IHtmlRenderer {
	public String render(String htmlTemplateString, Map<String, Object> data) {
		Template tmpl = Mustache.compiler().compile(htmlTemplateString);
		return tmpl.execute(data);
	}
}
