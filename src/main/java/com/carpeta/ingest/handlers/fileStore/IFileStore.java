package com.carpeta.ingest.handlers.fileStore;

import java.io.IOException;
import java.nio.file.Path;
import java.util.stream.Stream;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface IFileStore {

	public void init();

	public void save(MultipartFile file) throws IOException;

	public Resource load(String filename);
	
	public Resource loadDir(Path dir, String filename);

	public void deleteAll();

	public Stream<Path> loadAll();
}
