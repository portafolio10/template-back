package com.carpeta.ingest.handlers.templateFactory;

import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;
import com.carpeta.ingest.entity.Template;
import com.carpeta.ingest.entity.TemplateAttr;
import com.carpeta.ingest.entity.TemplateSchema;
import com.carpeta.ingest.requests.TemplateCreateRequest;

@Service
@Transactional
public class TemplateFactory implements ITemplateFactory {

	@PersistenceContext
	EntityManager em;

	@Override
	public Template create(TemplateCreateRequest command) {

		Template template = new Template(
				command.getNameTemplate(), 
				"http://some", 
				command.getContentTemplate(),
				command.getContentBase64Template());

		List<TemplateAttr> attrs = command.getAttrsTemplate()
				.stream()
				.map(attrName->{
					TemplateAttr attr = new TemplateAttr();
					attr.setName(attrName);
					attr.setTemplate(template);
					return attr;
				})
				.collect(Collectors.toList());		
		
		TemplateSchema schema = new TemplateSchema(command.getSchemaTemplate());
		
		template.setSchema(schema);
		template.setAttrs(attrs);
		
		em.persist(template);

		return template;
	}

}
