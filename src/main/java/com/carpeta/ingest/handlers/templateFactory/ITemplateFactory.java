package com.carpeta.ingest.handlers.templateFactory;

import com.carpeta.ingest.entity.Template;
import com.carpeta.ingest.requests.TemplateCreateRequest;

public interface ITemplateFactory {
	public Template create(TemplateCreateRequest command);
}
