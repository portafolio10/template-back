package com.carpeta.ingest.handlers.schemaValidator;

import org.json.JSONObject;

public interface ISchemaValidator {
	public boolean validate(String schemaString, JSONObject metaInfo);
}
