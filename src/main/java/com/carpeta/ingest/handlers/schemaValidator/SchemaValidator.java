package com.carpeta.ingest.handlers.schemaValidator;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.stereotype.Service;

@Service
public class SchemaValidator implements ISchemaValidator {

	public boolean validate(String schemaString, JSONObject metaInfo) {

		try {
			InputStream targetStream = new ByteArrayInputStream(schemaString.getBytes());
			JSONObject rawSchema = new JSONObject(new JSONTokener(targetStream));
			Schema schema = SchemaLoader.load(rawSchema);
			schema.validate(metaInfo);
			return true;
		} catch (ValidationException e) {
			return false;
		}
	}
}
