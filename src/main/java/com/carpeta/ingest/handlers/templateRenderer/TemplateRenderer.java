package com.carpeta.ingest.handlers.templateRenderer;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import javax.transaction.Transactional;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import com.carpeta.ingest.entity.Template;
import com.carpeta.ingest.exceptions.ValidSchemaKeysException;
import com.carpeta.ingest.exceptions.ValidSchemaValueException;
import com.carpeta.ingest.handlers.fileStore.IFileStore;
import com.carpeta.ingest.handlers.htmlRenderer.IHtmlRenderer;
import com.carpeta.ingest.handlers.htmlToPdfCoverter.IHtmlToPdfConverter;
import com.carpeta.ingest.handlers.schemaValidator.ISchemaValidator;
import com.carpeta.ingest.handlers.temporaryFileManager.ITemporaryFileManager;
import com.carpeta.ingest.repository.TemplateRepo;
import com.carpeta.ingest.requests.TemplateRenderRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class TemplateRenderer {

	
	@Autowired
	TemplateRepo templateRepo;
	
	@Autowired
	ISchemaValidator schemaValidator;
	
	@Autowired
	IHtmlRenderer htmlRenderer;
	
	@Autowired
	ITemporaryFileManager temporaryFileManager;
	
	@Autowired
	IHtmlToPdfConverter htmlToPdfConverter;
	
	@Autowired
	IFileStore fileStore;
	
	
	public Resource render(TemplateRenderRequest command) throws ValidSchemaKeysException, ValidSchemaValueException, IOException {
		Template templateX = this.find(command.getTemplateId());
		Map<String,Object> paramsStringToMap = this.getTemplateParamsToMap(command);
		this.validateSchemaKeysOrFail(templateX, paramsStringToMap);
		this.validateSchemaValueOrFail(templateX, command);
		String htmlRenderString = this.getHtmlRenderToString(templateX, paramsStringToMap);
		return this.generatePdfFromTemplate(htmlRenderString, templateX.getName());
	}
	
	@Transactional
	protected Template find(Long id) {
		Template templateX = this.templateRepo.findById(id).get();
		templateX.getAttrs();
		return templateX;
	}
	
	protected Map<String,Object> getTemplateParamsToMap(TemplateRenderRequest command) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> readValue = mapper.readValue(command.getTemplateParams(),new TypeReference<Map<String, Object>>() {});
		return readValue;
	}
	
	protected void validateSchemaKeysOrFail(Template templateX, Map<String,Object> paramsStringToMap) throws ValidSchemaKeysException {
		int count = (int) templateX.getAttrs()
				.stream()
				.filter(attr->paramsStringToMap.containsKey(attr.getName()))
				.count();
		if(paramsStringToMap.size() != count) throw new ValidSchemaKeysException();
	}
	
	protected void validateSchemaValueOrFail(Template template, TemplateRenderRequest command) throws ValidSchemaValueException {
		String schemaString = template.getSchema().getValue();
		JSONObject metaInfo = new JSONObject(command.getTemplateParams());
		if(!this.schemaValidator.validate(schemaString, metaInfo)) throw new ValidSchemaValueException();
	}
	
	
	protected String getHtmlRenderToString(Template template, Map<String,Object> paramsStringToMap) {
		return this.htmlRenderer.render(template.getContentParse(), paramsStringToMap);
	}
	
	protected Resource generatePdfFromTemplate(String htmlRenderString, String fileName) throws IOException {
		String fileNameTemplate = fileName.concat(".pdf");
		File templateHtml = this.temporaryFileManager.handle(fileName, htmlRenderString);
		this.htmlToPdfConverter.convert(templateHtml.toPath().toUri().toString(), fileNameTemplate);
		templateHtml.delete();
		return this.downloadTemplate(fileNameTemplate);
	}
	
	protected Resource downloadTemplate(String fileNameTemplate) {
		Path dir = Paths.get("template_to_pdf");
		return this.fileStore.loadDir(dir, fileNameTemplate);
	}

}
