package com.carpeta.ingest.handlers.templateRenderer;

import com.carpeta.ingest.entity.Template;
import com.carpeta.ingest.requests.TemplateRenderRequest;

public interface ITemplateRenderer {

	public Template render(TemplateRenderRequest command);
}
