package com.carpeta.ingest.handlers.templateCrud;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.carpeta.ingest.entity.Template;
import com.carpeta.ingest.repository.TemplateRepo;

@Service
public class TemplateCrud implements ITemplateCrud {

	
	@PersistenceContext
	EntityManager em;
	
	@Autowired
	private TemplateRepo templateRepo;
	
	
	@Override
	@Transactional(readOnly = true)
	public List<Template> list() {
		return this.templateRepo.findAll();
	}

	@Override
	@Transactional
	public Template findOrFail(long id) {
		Template templateX = this.templateRepo.findById(id).get();
		return templateX;
	}

}
