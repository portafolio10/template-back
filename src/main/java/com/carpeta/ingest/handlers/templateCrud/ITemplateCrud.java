package com.carpeta.ingest.handlers.templateCrud;
import java.util.List;

import com.carpeta.ingest.entity.Template;

public interface ITemplateCrud {
	public List<Template> list();
	public Template findOrFail(long id);
}
