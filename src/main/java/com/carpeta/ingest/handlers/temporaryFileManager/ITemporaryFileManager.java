package com.carpeta.ingest.handlers.temporaryFileManager;

import java.io.File;
import java.io.IOException;

public interface ITemporaryFileManager {
	public File handle(String htmlFileName, String htmlContent) throws IOException;
}
