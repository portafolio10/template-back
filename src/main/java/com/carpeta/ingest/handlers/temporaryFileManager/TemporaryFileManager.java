package com.carpeta.ingest.handlers.temporaryFileManager;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.springframework.stereotype.Service;

@Service
public class TemporaryFileManager implements ITemporaryFileManager {
	
	public File handle(String htmlFileName, String htmlContent) throws IOException {
        File f = File.createTempFile(htmlFileName,".html");
 
        BufferedWriter bw = new BufferedWriter(new FileWriter(f));
        bw.write(htmlContent);
        bw.close();
 
		return f;
	}
	
}
