package com.carpeta.ingest.handlers.htmlToPdfCoverter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import org.springframework.stereotype.Service;

import com.openhtmltopdf.pdfboxout.PdfRendererBuilder;

@Service
public class HtmlToPdfConverter implements IHtmlToPdfConverter {

	private final String root = "template_to_pdf/";

	public String convert(String htmlTemplateUri, String pdfName) {
		try (OutputStream os = new FileOutputStream(this.getFilePath(pdfName))) {
			PdfRendererBuilder builder = new PdfRendererBuilder();
			builder.useFastMode();
			builder.withUri(htmlTemplateUri);
			builder.toStream(os);
			builder.run();
			return "todo fluyo";
		} catch (FileNotFoundException e) {
			return "Archivo no encontrado" + e.getMessage();
		} catch (IOException e) {
			return "error i/o" + e.getMessage();
		}
	}
	
	protected String getFilePath(String fileName) {
		return root.concat(fileName);
	}

}
