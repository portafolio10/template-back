package com.carpeta.ingest.handlers.htmlToPdfCoverter;

public interface IHtmlToPdfConverter {
	public String convert(String htmlTemplateUri, String pdfName);
}
