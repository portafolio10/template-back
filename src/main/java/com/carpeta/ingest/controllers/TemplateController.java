package com.carpeta.ingest.controllers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.carpeta.ingest.entity.Template;
import com.carpeta.ingest.exceptions.ValidSchemaKeysException;
import com.carpeta.ingest.exceptions.ValidSchemaValueException;
import com.carpeta.ingest.handlers.templateCrud.ITemplateCrud;
import com.carpeta.ingest.handlers.templateFactory.ITemplateFactory;
import com.carpeta.ingest.handlers.templateRenderer.TemplateRenderer;
import com.carpeta.ingest.requests.TemplateCreateRequest;
import com.carpeta.ingest.requests.TemplateRenderRequest;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class TemplateController {

	@Autowired
	ITemplateFactory templateFactory;
	
	@Autowired
	TemplateRenderer templateRenderer;
	
	@Autowired
	ITemplateCrud templateCrud;
	
	@PostMapping("/template/create")
	public Template createTemplate(@RequestBody TemplateCreateRequest templateCreateRequest) {
		return this.templateFactory.create(templateCreateRequest);
	}
	
	@PostMapping("/template/render")
	public ResponseEntity<Resource> renderTemplate(@RequestBody TemplateRenderRequest templateRenderRequest) throws ValidSchemaKeysException, ValidSchemaValueException, IOException {
		Resource file = this.templateRenderer.render(templateRenderRequest);
		Path path = file.getFile().toPath();
		 return ResponseEntity.ok()
				 .header(HttpHeaders.CONTENT_TYPE, Files.probeContentType(path))
                 .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                 .body(file);
	}
	
	@GetMapping("/template/all")
	public List<Template> findAll() {
		return this.templateCrud.list();
	}
	
	@GetMapping("/template/find")
	public Template findTemplate(@RequestParam long id) {
		return this.templateCrud.findOrFail(id);
	}
}
