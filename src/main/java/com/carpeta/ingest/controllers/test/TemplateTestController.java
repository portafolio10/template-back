package com.carpeta.ingest.controllers.test;


import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.carpeta.ingest.handlers.fileStore.IFileStore;
import com.carpeta.ingest.handlers.htmlRenderer.IHtmlRenderer;
import com.carpeta.ingest.handlers.htmlToPdfCoverter.IHtmlToPdfConverter;
import com.carpeta.ingest.handlers.schemaValidator.ISchemaValidator;
import com.carpeta.ingest.handlers.templateFactory.ITemplateFactory;
import com.carpeta.ingest.handlers.temporaryFileManager.ITemporaryFileManager;

@RestController
public class TemplateTestController {

	@Autowired
	IFileStore fileStore;

	@Autowired
	ISchemaValidator schemaValidator;
	
	@Autowired
	IHtmlToPdfConverter htmlToPdfConverter;

	@Autowired
	IHtmlRenderer htmlRenderer;
	
	@Autowired
	ITemporaryFileManager temporaryFileManager;
	
	@Autowired
	ITemplateFactory templateFactory;
	
	@GetMapping("/test/validate-schema")
	public String saluda() {
		String schemaString = "{\"type\":\"object\",\"properties\":{\"rectangle\":{\"$ref\":\"#/definitions/Rectangle\"}},\"definitions\":{\"size\":{\"type\":\"number\",\"minimum\":0},\"Rectangle\":{\"type\":\"object\",\"properties\":{\"a\":{\"$ref\":\"#/definitions/size\"},\"b\":{\"$ref\":\"#/definitions/size\"}}}}}";
		JSONObject metaInfo = new JSONObject("{\"rectangle\":{\"a\":5,\"b\":3}}");
		return this.schemaValidator.validate(schemaString, metaInfo) ? "valido" : "invalido";
	}

	@PostMapping("/test/upload-file")
	public String uploadFile(@RequestParam("file") MultipartFile file) throws IOException {
		String message = "";
		fileStore.save(file);
		message = "Uploaded the file successfully: " + file.getOriginalFilename();
		return message;
	}

	@GetMapping("/test/convert-html-to-pdf")
	public String convertHtmlToPdf() throws IOException {
		Path root = Paths.get("template_uploads");
		Path template = root.resolve("container.html");		
		return this.htmlToPdfConverter.convert(template.toUri().toString(), "miArchivo.pdf");
	}
	
	@GetMapping("/test/parse-html")
	public String parseHtml() {
		String templateString = "One, two, {{three}}. Three sir!";
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("three", "five");
		return this.htmlRenderer.render(templateString, data);
	}
	
	@GetMapping("/test/manejo-archivo-temporal")
	public String manejoArchivosTemporales() throws IOException {
		File templateHtml = this.temporaryFileManager.handle("archivoTemporal_", "<div>Hola mundo wey</div>");
		this.htmlToPdfConverter.convert(templateHtml.toPath().toUri().toString(), "miTemplate.pdf");
		templateHtml.delete();
		return "Todo fluyo";
	}

}
