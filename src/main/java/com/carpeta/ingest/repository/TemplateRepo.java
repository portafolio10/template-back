package com.carpeta.ingest.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.carpeta.ingest.entity.Template;

public interface TemplateRepo extends JpaRepository<Template, Long>{

}
