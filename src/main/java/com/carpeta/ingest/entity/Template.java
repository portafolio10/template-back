package com.carpeta.ingest.entity;

import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.util.List;

@Entity
public class Template {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	protected Long id;
	protected String name;
	protected String url;
	@Column(name = "contentParse", columnDefinition="VARCHAR(900)")
	protected String contentParse;
	@Column(name = "contentBase64", columnDefinition="VARCHAR(900)")
	protected String contentBase64;
	
	@JsonManagedReference
	@OneToMany(mappedBy = "template", cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	protected List<TemplateAttr> attrs;
	
	@OneToOne(cascade = CascadeType.PERSIST)
	protected TemplateSchema schema;
	
	public Template() {
	}
	
	public Template(String name, String url, String contentParse, String contentBase64) {
		this.name = name;
		this.url = url;
		this.contentParse = contentParse;
		this.contentBase64 = contentBase64;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getContentParse() {
		return contentParse;
	}
	
	public void setContentParse(String contentParse) {
		this.contentParse = contentParse;
	}
	
	public String getContentBase64() {
		return contentBase64;
	}
	
	public void setContentBase64(String contentBase64) {
		this.contentBase64 = contentBase64;
	}
	
	public List<TemplateAttr> getAttrs() {
		return attrs;
	}

	public void setAttrs(List<TemplateAttr> attrs) {
		this.attrs = attrs;
	}
	
	

	public TemplateSchema getSchema() {
		return schema;
	}

	public void setSchema(TemplateSchema schema) {
		this.schema = schema;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Template))
			return false;
		Template other = (Template) obj;
		return Objects.equals(id, other.id);
	}
}
